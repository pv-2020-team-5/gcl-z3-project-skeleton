# gcl-z3-skeleton

This repository contains a skeleton which makes it slightly easier to get started for the Program Verification project.

## What do I do?

1. Go to the [Z3 releases page] and download a release of Z3 4.x.x that is appropriate for your system.
   This should be a .zip file named something like `z3-4.8.9-x64-ubuntu-16.04.zip`.
2. Extract the zip file's contents to the root of this repository.
   This should give you a folder with the same name as the .zip file,
   so `z3-4.8.9-x64-ubuntu-16.04` in the case of the example above.
3. Rename this folder to be called `z3-installation`.
   (This is not strictly necessary but makes the next step easier.)
3. Run `stack build --extra-lib-dirs z3-installation/bin --extra-include-dirs z3-installation/include`.

   This will:
     - Download the [GCL parser library] and compile it
     - Download the Z3 bindings and compile them
     - Download a bunch of other packages that are dependencies of one of the two previous packages.

If this exits without an error, you're good to go!

After compiling the project once with the `--extra-lib-dirs` and `--extra-include-dirs` flags
you should be able to build with just `stack build`, as long as you don't (re)move your Z3 installation.

The rest of this repository contains the default result of running `stack new`, the altered files are:
 - `stack.yaml`: To clean up some comments and add the Z3/GCLparser dependencies
 - `package.yaml`: To use those dependencies
 - `.gitignore`: To ignore a downloaded Z3 installation

Happy hacking!

[Z3 releases page]: https://github.com/Z3Prover/z3/releases
[GCL parser library]: https://github.com/wooshrow/gclparser
[Z3 bindings]: https://hackage.haskell.org/package/z3
